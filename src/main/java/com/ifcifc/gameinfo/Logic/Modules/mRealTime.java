package com.ifcifc.gameinfo.Logic.Modules;

import com.ifcifc.gameinfo.Logic.ModuleController.ModuleBase;
import com.ifcifc.gameinfo.Logic.ModuleController.ModuleRegNameAnnotation;
import com.ifcifc.gameinfo.Logic.ModuleController.ModuleRegisterAnnotation;
import net.minecraft.client.MinecraftClient;

import java.time.LocalDateTime;

@ModuleRegNameAnnotation(RegName="RealTime")
public class mRealTime implements ModuleBase {

    LocalDateTime local;
    public boolean isEnable = true;

    public mRealTime() {
        local =  LocalDateTime.now();
    }


    @Override
    public void setEnable(boolean v) {
        isEnable = v;
    }

    @Override
    public boolean isEnable() {
        return isEnable;
    }

    @Override
    public void update(MinecraftClient tick) {
        if(!isEnable)return;
        local =  LocalDateTime.now();
    }
    @ModuleRegisterAnnotation(Funcion = "RTHour", description = "Real hour")
    public int getHour(){
        return local.getHour();
    }
    @ModuleRegisterAnnotation(Funcion = "RTMinute", description = "Real minute")
    public int getMinute(){
        return local.getMinute();
    }
    @ModuleRegisterAnnotation(Funcion = "RTSecond", description = "Real second")
    public int getSecond(){
        return local.getSecond();
    }

    @ModuleRegisterAnnotation(Funcion = "RTFixHour", description = "Real hour, if less than 10, Add a 0 to the front")
    public String getFixHour(){
        final int v = local.getHour();
        return ((v<10)? "0": "") + v;
    }
    @ModuleRegisterAnnotation(Funcion = "RTFixMinute", description = "Real minute, if less than 10, Add a 0 to the front")
    public String getFixMinute(){
        final int v = local.getMinute();
        return ((v<10)? "0": "") + v;
    }
    @ModuleRegisterAnnotation(Funcion = "RTFixSecond", description = "Real second, if less than 10, Add a 0 to the front")
    public String getFixSecond(){
        final int v = local.getSecond();
        return ((v<10)? "0": "") + v;
    }

    public boolean isHidden() {
        return !isEnable ;
    }
}
