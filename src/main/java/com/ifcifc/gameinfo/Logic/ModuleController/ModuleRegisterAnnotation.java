package com.ifcifc.gameinfo.Logic.ModuleController;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface ModuleRegisterAnnotation {
    String  Funcion()            default "default";
    String  isHiddenFuncion()    default "isHidden";
    String  defaultArguments()   default "";
    boolean acceptArguments()    default false;
    boolean isNeededArguments()  default false;


    String  description();
    String  Arguments()          default "";

}
