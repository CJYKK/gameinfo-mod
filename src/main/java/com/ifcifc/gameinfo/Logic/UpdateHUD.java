package com.ifcifc.gameinfo.Logic;

import com.ifcifc.gameinfo.Config.Config;
import com.ifcifc.gameinfo.Logic.ModuleController.ModulesRegister;
import com.ifcifc.gameinfo.Logic.Modules.CheckDeath;
import com.ifcifc.gameinfo.Logic.Modules.LightAlert;
import com.ifcifc.gameinfo.Render.RenderHUD;
import com.ifcifc.gameinfo.Util.PackageDebug;
import jdk.nashorn.api.scripting.ScriptObjectMirror;
import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientTickEvents;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.resource.language.LanguageDefinition;
import net.minecraft.util.Language;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class UpdateHUD {

    public static CheckDeath    checkDeath = new CheckDeath();
    public static LightAlert    lightAlert = new LightAlert();

    public static ScriptEngine  Engine;
    public static ScriptObjectMirror result = null;

    public static boolean isPause = false;
    public static boolean isLanUpdate = false;

    private static LanguageDefinition Lan= null;

    public static void initialize(){
        ScriptEngineManager manager = new ScriptEngineManager();
        Engine = manager.getEngineByName("nashorn");
        ClientTickEvents.START_CLIENT_TICK.register(tick->{
            if(isPause || null == tick.world || null == tick.player || tick.isPaused())return;

            if(checkTranslate()){
                isLanUpdate = true;
                UpdateHUD.reload();
                RenderHUD.toggleHUD();
            }

            checkDeath.checkDeath(tick);
            lightAlert.update(tick);

            if(checkIfHidden())return;

            ModulesRegister.ModulesList.forEach(v-> v.update(tick));

            Invocable invocable = (Invocable) UpdateHUD.Engine;
            try {
                result = (ScriptObjectMirror)invocable.invokeFunction("Update");
            } catch (ScriptException | NoSuchMethodException e) {
                e.printStackTrace();
            }

            RenderHUD.Lines.forEach(L-> {
                try {
                    L.update(tick);
                }catch (Exception E){
                    L.setText("Line ERROR");
                    E.printStackTrace();
                }
            });
            RenderHUD.toggleHUD();

        });
    }

    public static void reload(){
        isPause=true;
        ScriptEngineManager manager = new ScriptEngineManager();
        RenderHUD.Lines.clear();
        UpdateHUD.Engine = manager.getEngineByName("nashorn");


        if(Config.options.DebugPackages){
            PackageDebug.update();
        }else{
            LoadHUD.initialize();
        }
        isPause=false;
        System.out.println("Update HUD");
    }

    public static boolean checkIfHidden(){
        return Config.options.Hidden || MinecraftClient.getInstance().options.debugEnabled;
    }

    public static boolean checkTranslate(){
        String key = "text.gameinfo.day";
        final LanguageDefinition language = MinecraftClient.getInstance().getLanguageManager().getLanguage();
        if(!language.equals(Lan)){
            Lan = language;
            return true;
        }
        return !isLanUpdate && !key.equals(Language.getInstance().get(key));
    }
}
