package com.ifcifc.gameinfo.Logic.Modules;

import com.ifcifc.gameinfo.Config.Config;
import com.ifcifc.gameinfo.Logic.ModuleController.ModuleBase;
import com.ifcifc.gameinfo.Logic.ModuleController.ModuleRegNameAnnotation;
import com.ifcifc.gameinfo.Logic.ModuleController.ModuleRegisterAnnotation;
import com.ifcifc.gameinfo.mixin.CurrentFPSMixin;
import com.ifcifc.gameinfo.mixin.ServerTicks;
import net.minecraft.client.MinecraftClient;

@ModuleRegNameAnnotation(RegName="TickFPS")
public class mTickFPSClient implements ModuleBase {
    public boolean isEnable = true;

    int FPS, Tick;
    @Override
    public void update(MinecraftClient tick) {
        if(!isEnable)return;
        if (tick.isIntegratedServerRunning()) {
            Tick = (int)((ServerTicks) tick.getServer()).getTickTime();
        }else{
            Tick = 0;
        }


        FPS = ((CurrentFPSMixin)tick).getCurrentFps();
    }


    @Override
    public void setEnable(boolean v) {
        isEnable = v;
    }

    @Override
    public boolean isEnable() {
        return isEnable;
    }

    @ModuleRegisterAnnotation(Funcion = "FPS", description = "Client FPS")
    public int getFPS(){
        return FPS;
    }

    @ModuleRegisterAnnotation(Funcion = "Tick", isHiddenFuncion="isHiddenTick", description = "Integrated server ticks")
    public int getTick(){
        return Tick;
    }

    @Override
    public boolean isHidden() {
        return !isEnable || Config.options.HiddenFPS;
    }

    public boolean isHiddenTick() {
        return  !isEnable || Config.options.HiddenTicks || !MinecraftClient.getInstance().isIntegratedServerRunning();
    }


}
