package com.ifcifc.gameinfo.Util;

import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.LightType;
import net.minecraft.world.chunk.WorldChunk;
import net.minecraft.world.dimension.DimensionType;

public class WorldUtil {
    public ClientWorld world;

    public WorldUtil(ClientWorld world) {
        this.world = world;
    }

    public WorldUtil() {
        this.world = MinecraftClient.getInstance().world;
    }

    public boolean dimencionEqual(Identifier dim){

        return world.getRegistryKey().getValue().equals(dim);
    }

    public boolean isOverworld(){
        return dimencionEqual(DimensionType.OVERWORLD_ID);
    }
    public boolean isNether(){
        return dimencionEqual(DimensionType.THE_NETHER_ID);
    }
    public boolean isEnd(){
        return dimencionEqual(DimensionType.THE_END_ID);

    }

    public String getDimensionName(){

		if(this.isOverworld())return "Overworld";
		if(this.isNether())return "Nether";
		if(this.isEnd())return "End";

        return "Other";
    }

    public static String getLevelName() {
        try {
            return (MinecraftClient.getInstance().isIntegratedServerRunning())?
                    MinecraftClient.getInstance().getServer().getIconFile().getParentFile().getName()://FIX WORLD PATH?
                    MinecraftClient.getInstance().getCurrentServerEntry().address;
        } catch (Exception e) {
            return "default";
        }
    }

    public static int[] getLight(){
        int[] ret = {0,0};
        try {
            MinecraftClient tick = MinecraftClient.getInstance();

            final ClientPlayerEntity player = tick.player;

            final WorldChunk chunk = tick.world.getChunk(player.chunkX, player.chunkZ);

            if(null!=chunk && null != chunk.getLightingProvider()){

                BlockPos pPos = new BlockPos(player.getPos());
                ret[0] = chunk.getLightingProvider().get(LightType.BLOCK).getLightLevel(pPos);
                ret[1] = chunk.getLightingProvider().get(LightType.SKY).getLightLevel(pPos);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return ret;
    }
}
