package com.ifcifc.gameinfo.Logic.ModuleController;

import net.minecraft.client.MinecraftClient;

public interface ModuleBase {

    public void update(MinecraftClient tick);
    public boolean isHidden();

    public void setEnable(boolean v);
    public boolean isEnable();
}
