package com.ifcifc.gameinfo.Logic.Modules;

import com.ifcifc.gameinfo.Config.Config;
import com.ifcifc.gameinfo.Util.WorldUtil;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.toast.SystemToast;
import net.minecraft.sound.SoundEvents;
import net.minecraft.text.LiteralText;

public class LightAlert{
    private int  delay=20*5;
    private int  toast_delay=0;
    private int  sound_delay=0;

    public void update(MinecraftClient tick) {
        if (delay > 0) delay--;
        if (toast_delay > 0) toast_delay--;
        if (sound_delay > 0) sound_delay--;

        if (!Config.options.AlertLowLightLevel || delay != 0) return;
        final int[] light = WorldUtil.getLight();
        final int level = (Config.options.LowLevelSunAndBlock) ? Math.max(light[0], light[1]) : light[0];
        if (level <= Config.options.AlertMinLightLevel) {
            delay = Config.options.AlertTickDelay;
            if (sound_delay == 0 && Config.options.PlaySoundAlertLowLevel && level <= Config.options.SoundAlertMinLightLevel) {
                sound_delay = 25;
                tick.player.playSound(SoundEvents.ENTITY_GHAST_SCREAM, 1f, 1f);
            }
            if (toast_delay == 0) {
                toast_delay = 140;
                if (Config.options.ToastAlertLowLight)
                    tick.getToastManager().add(new SystemToast(null, new LiteralText("Light Level Alert"), new LiteralText("Light Low Level: " + level)));
            }
        }
    }

}