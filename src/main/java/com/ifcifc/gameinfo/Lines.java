package com.ifcifc.gameinfo;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ifcifc.gameinfo.Util.LineBuilder.Utils.DimensionFormat;
import com.ifcifc.gameinfo.Util.LineBuilder.Utils.LineClass;
import com.ifcifc.gameinfo.Util.LineBuilder.Utils.LineFormat;
import com.ifcifc.gameinfo.Util.LineBuilder.Utils.LinePackageClass;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Lines {

    public static LineClass Line1(){

        LineFormat LF1 = new LineFormat(false, false, "§e$Translated$: §r","text.gameinfo.day","", "", "Day", "", "", "");

        LineFormat LF2 = new LineFormat(false, false, "§e$Translated$: §r","text.gameinfo.time"," ", "", "FixHour", "", "", "");

        LineFormat LF3 = new LineFormat(false,"","§e:§r","","FixMinute", "", "LineMinute");
        
        LineFormat LF4 = new LineFormat(false, true, "§e$Translated$: §r","text.gameinfo.light"," ", "", "LightFixSun", "true", "", "");
        LineFormat LF5 = new LineFormat(false, false, "","text.gameinfo.light","§r/", " §e$Translated$: §r", "LightFixBlock", "true", "", "");


        DimensionFormat DF = new DimensionFormat(new LineFormat[]{LF1,LF2,LF3,LF4,LF5}, "default");

        return new LineClass("Line1", 1, new DimensionFormat[]{DF});
    }

    public static LineClass Line2(){
        LineFormat LF1 = new LineFormat("§eXYZ: §f","", "RCoords");
        LineFormat LF2 = new LineFormat("", "§e - §f", "FixDireccion");
        //LineFormat LF3 = new LineFormat("Biome: §f", " §e - ", "Biome");

        LineFormat LF3 = new LineFormat(false, false, "$Translated$: §f","text.gameinfo.biome"," §e - ", "", "Biome", "", "", "");
        LineFormat LF4 = new LineFormat(false, false, "$Translated$: §r","text.gameinfo.speed","§e - ", "", "VelBs", "", " B/s", "");

        //LineFormat LF4 = new LineFormat(false, false,"Speed: §r", "§e - ", "", "VelBs", "", " B/s", "");
        LineFormat LF5 = new LineFormat("", " §e§l- §r§a", "Slime");

        DimensionFormat DF = new DimensionFormat(new LineFormat[]{LF1,LF2,LF3,LF4,LF5}, "default");

        return new LineClass("Line2", 2, new DimensionFormat[]{DF});
    }

    public static LineClass Line3(){
        LineFormat LF1 = new LineFormat("§eN-XZ: §f", "", "NRCoords");
        LineFormat LF2 = new LineFormat("§eO-XZ: §f", "", "ORCoords");

        DimensionFormat DF  = new DimensionFormat(new LineFormat[]{LF1}, "default");
        DimensionFormat DF2 = new DimensionFormat(new LineFormat[]{LF2}, "the_nether");

        return new LineClass("Line3", 3, new DimensionFormat[]{DF, DF2});
    }
    public static LineClass Line4(){
        LineFormat LF1 = new LineFormat("§eFPS: §r","", "FPS");
        LineFormat LF2 = new LineFormat(false,"§eTick: §r"," ","","Tick", "", "LineTick");

        //LineFormat LF3 = new LineFormat(false,"§eScore: §r"," ","","Score", "'test'", "LineTick");
        //LineFormat LF4 = new LineFormat(false,""," ","","XPLevel", "", "LineTick");
        //LineFormat LF5 = new LineFormat(false,""," ","","XPProgress", "", "LineTick");
        //LineFormat LF6 = new LineFormat(false,""," ","","XPTotal", "", "LineTick");

        //DimensionFormat DF = new DimensionFormat(new LineFormat[]{LF1,LF2,LF3,LF4,LF5,LF6}, "default");
        DimensionFormat DF = new DimensionFormat(new LineFormat[]{LF1,LF2}, "default");

        return new LineClass("Line4", 0, new DimensionFormat[]{DF});
    }

    public static LineClass Line5(){
        LineFormat LF1 = new LineFormat("", "", "RTFixHour");
        LineFormat LF2 = new LineFormat("§e:§f", "", "RTFixMinute");
        LineFormat LF3 = new LineFormat("§e:§f", "", "RTFixSecond");

        DimensionFormat DF = new DimensionFormat(new LineFormat[]{LF1,LF2,LF3}, "default");

        return new  LineClass("Line5", 2, 2, 2,false, new DimensionFormat[]{DF});
    }

    public static LineClass Line6(){
        LineFormat LF1 = new LineFormat("Tessstt");

        DimensionFormat DF = new DimensionFormat(new LineFormat[]{LF1}, "default");

        return new LineClass("Line6", 0, new DimensionFormat[]{DF});
    }

    public static void makePackage(File file){
        final LineClass[] LC = {Lines.Line1(), Lines.Line2(), Lines.Line3(), Lines.Line4()};

        LinePackageClass LPC = new LinePackageClass("defaultHUD", "ifcifc", "", LC);

        try {
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            FileWriter sFile;
            sFile = new FileWriter(file);
            sFile.write(gson.toJson(LPC));
            sFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void saveJSON(LineClass LC){
        try {
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            FileWriter sFile;
            sFile = new FileWriter(new File("/home/igna/Documentos/gameinfo-mod/", LC.Name+".json"));
            sFile.write(gson.toJson(LC));
            sFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void saveJS(String Code, String Name){
        try {
            FileWriter sFile;
            sFile = new FileWriter(new File("/home/igna/Documentos/gameinfo-mod/"+Name+".js"));
            sFile.write(Code);
            sFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
