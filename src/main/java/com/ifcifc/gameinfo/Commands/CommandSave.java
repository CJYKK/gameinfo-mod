package com.ifcifc.gameinfo.Commands;

import com.ifcifc.gameinfo.Config.Config;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import net.minecraft.server.command.CommandManager;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.LiteralText;

public class CommandSave {
    public static void register(LiteralArgumentBuilder<ServerCommandSource> literal) {
        literal.then(CommandManager.literal("save") .executes(CommandSave::execute));
    }

    private static int execute(CommandContext<ServerCommandSource> context) {
        Config.updateSave();
        context.getSource().sendFeedback(new LiteralText("Save config"), false);

        return 0;
    }
}
