package com.ifcifc.gameinfo.Util.LineBuilder.Utils;

public class LineClass {
    public final String Name;
    public final int Index;
    public final float ScaleX;
    public final float ScaleY;
    public final boolean useDefaultScale;
    public final DimensionFormat[] Line;

    public LineClass(String name, int index, float scaleX, float scaleY, boolean useDefaultScale, DimensionFormat[] line) {
        Name = name;
        Index = index;
        ScaleX = scaleX;
        ScaleY = scaleY;
        this.useDefaultScale = useDefaultScale;
        Line = line;
    }

    public LineClass(String name, int index, DimensionFormat[] line) {
        this(name, index,1,1, true, line);
    }

}
